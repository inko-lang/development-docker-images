# Development Docker Images

This repository contains various Docker files that are used for building images
for Inko's development cycle. For the most part they are standard Docker images
(e.g. `alpine:edge`) with various dependencies installed.

Images are rebuilt automatically once a day.

## License

All source code in this repository is licensed under the Mozilla Public License
version 2.0, unless stated otherwise. A copy of this license can be found in the
file "LICENSE".
